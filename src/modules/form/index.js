
import ng from 'angular';
import formController from './controllers/form'

import { readFileSync } from 'fs';
let formTemplate = readFileSync(`${__dirname}/templates/form.html`, 'utf8');

export default ng
  .module('formModule', ['ngRoute'])
  .run(['$templateCache', ($templateCache)=>{
    $templateCache.put('formTemplate', formTemplate)
  }])
  .config(['$routeProvider', ($roteProvider)=>{
    $roteProvider
      .when('/', {
        controller: formController,
        template: formTemplate
      })
  }])
