import './modules'
import ng from 'angular';
import 'angular-route'

let appDeps = [
  'ngRoute',
  'formModule'
];

let app = ng
  .module('someTest', appDeps)
  .controller('mainController', ['$scope', '$route', '$location', '$rootScope', ($scope, $route, $location) => {
    $scope.$on('$locationChangeSuccess',() => {
      console.log('routeChangeSuccess');
      console.log($location.path());
    })
  }]);

let $html = ng.element(document.getElementsByTagName('html')[0]);
ng.element().ready(() => {
  ng.bootstrap($html, [app.name])
});