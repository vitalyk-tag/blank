var gulp = require('gulp');

var watchScripts = require('./tasks/scripts').watchScripts;
var watchScriptsDev = require('./tasks/scripts').watchScriptsDev;
var watchHtml = require('./tasks/htmls').watchHtml;
var watchStyles = require('./tasks/styles').watchStyles;
var watchSprites = require('./tasks/sprites').watchSprites;
var browserSync = require('browser-sync').create();

var browserWatch = ()=> {
  browserSync.init({
    server: 'build'
  });

  browserSync.watch('build/**/*.*').on('change', browserSync.reload);
};

gulp.task('watch', ['build'], ()=> {
  watchScripts();
  watchHtml();
  watchStyles();
  watchSprites();

  browserWatch()
});

gulp.task('watch-dev', ['build-dev'], ()=> {
  watchScriptsDev();
  watchHtml();
  watchStyles();
  watchSprites();

  browserWatch()
});