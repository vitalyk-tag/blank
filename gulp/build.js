require('./tasks/scripts');
require('./tasks/htmls');
require('./tasks/styles');

var gulp = require('gulp');

gulp.task('build', ['scripts', 'html', 'style']);
gulp.task('build-dev', ['scripts-dev', 'html', 'style']);