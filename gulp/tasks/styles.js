var gulp = require('gulp');
var del = require('del');
var sass = require('gulp-sass');
var concat = require('gulp-concat');

require('./sprites');

var conf = {
  outputStyle: 'compressed',
  errLogToConsole : true,
  includePaths : [
    'tmp/sprites'
  ]
};

var src = [
  './src/styles/layout.scss',
  './src/styles/*.scss',
  './src/**/*.scss'
];
var dest = './build/css/';
var cssName = 'style.css';

gulp.task('style', ['sprite'], ()=> {
  del([dest]);

  return gulp.src(src)
    .pipe(sass(conf).on('error', sass.logError))
    .pipe(concat(cssName))
    .pipe(gulp.dest(dest));
});

exports.watchStyles = ()=> {
  gulp.watch(src, ['style']);
};