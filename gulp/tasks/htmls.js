var gulp = require('gulp');
var del = require('del');

var src = ['./src/*.html'];

gulp.task('html', ()=>{
  del(['./build/*.html']);
  return gulp.src(src)
    .pipe(gulp.dest('./build'))
});

exports.watchHtml = ()=>{
  gulp.watch(src, ['html']);
};