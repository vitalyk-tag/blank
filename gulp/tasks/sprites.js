var gulp = require('gulp');
var sprite = require('gulp.spritesmith');
var merge = require('merge-stream');
var del = require('del');

var conf = {
  imagesSrc: './src/images/icons/*',
  scssDest: './tmp/sprites',
  imgDest: './build/images',
  sprite: {
    imgName: 'sprite.png',
    cssName: 'sprite.scss',
    imgPath: '../images/sprite.png'
  }
};

gulp.task('sprite', ()=> {
  del(['tmp/sprites/*.*', 'build/images/sprite.png']);

  var spriteData = gulp.src(conf.imagesSrc).pipe(sprite(conf.sprite));
  var streamImg = spriteData.img.pipe(gulp.dest(conf.imgDest));
  var streamScss = spriteData.css.pipe(gulp.dest(conf.scssDest));

  return merge(streamImg, streamScss);
});

exports.watchSprites = ()=> {
  gulp.watch(conf.imagesSrc, ['style']);
};