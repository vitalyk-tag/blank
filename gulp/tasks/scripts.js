var gulp = require('gulp');
var browserify = require('browserify');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var uglify = require('gulp-uglify');
//var sourceMap = require('gulp-sourcemaps');
var del = require('del');

var conf = {
  src: './src',
  srcFile: 'app.js',
  dest: './build',
  destFile: 'app.js'
};

function bundle(debug){
  return browserify(`${conf.src}/${conf.srcFile}`, {debug: !!debug})
    .bundle()
}

gulp.task('scripts', ()=> {
  //del([`${conf.dest}/${conf.destFile}`]);
  return bundle()
    .pipe(source(`${conf.destFile}`))
    .pipe(buffer())
    .pipe(uglify({mangle: true}))
    .pipe(gulp.dest(conf.dest));
});

gulp.task('scripts-dev', ()=> {
  //del([`${conf.dest}/${conf.destFile}`]);
  return bundle(true)
    .pipe(source(`${conf.destFile}`))
    .pipe(buffer())
    .pipe(gulp.dest(conf.dest));
});

exports.watchScripts = ()=> {
  gulp.watch([`${conf.src}/${conf.srcFile}`, `${conf.src}/modules/**/*.js`], ['scripts'])
};

exports.watchScriptsDev = ()=> {
  gulp.watch([`${conf.src}/${conf.srcFile}`, `${conf.src}/modules/**/*.js`], ['scripts-dev'])
};